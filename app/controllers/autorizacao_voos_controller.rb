class AutorizacaoVoosController < ApplicationController
  before_action :set_autorizacao_voo, only: [:show, :edit, :update, :destroy]

  # GET /autorizacao_voos
  # GET /autorizacao_voos.json
  def index
    @autorizacao_voos = AutorizacaoVoo.all
  end

  # GET /autorizacao_voos/1
  # GET /autorizacao_voos/1.json
  def show
  end

  # GET /autorizacao_voos/new
  def new
    @plano_de_voos = PlanoDeVoo.all
    @autorizacao_voo = AutorizacaoVoo.new
  end

  # GET /autorizacao_voos/1/edit
  def edit
    @plano_de_voos = PlanoDeVoo.all
  end

  # POST /autorizacao_voos
  # POST /autorizacao_voos.json
  def create
    @autorizacao_voo = AutorizacaoVoo.new(autorizacao_voo_params)

    respond_to do |format|
      if @autorizacao_voo.save
        format.html { redirect_to @autorizacao_voo, notice: 'Autorizacao voo was successfully created.' }
        format.json { render :show, status: :created, location: @autorizacao_voo }
      else
        format.html { render :new }
        format.json { render json: @autorizacao_voo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /autorizacao_voos/1
  # PATCH/PUT /autorizacao_voos/1.json
  def update
    respond_to do |format|
      if @autorizacao_voo.update(autorizacao_voo_params)
        format.html { redirect_to @autorizacao_voo, notice: 'Autorizacao voo was successfully updated.' }
        format.json { render :show, status: :ok, location: @autorizacao_voo }
      else
        format.html { render :edit }
        format.json { render json: @autorizacao_voo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /autorizacao_voos/1
  # DELETE /autorizacao_voos/1.json
  def destroy
    @autorizacao_voo.destroy
    respond_to do |format|
      format.html { redirect_to autorizacao_voos_url, notice: 'Autorizacao voo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_autorizacao_voo
      @autorizacao_voo = AutorizacaoVoo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def autorizacao_voo_params
      params.require(:autorizacao_voo).permit(:tipo_autorizacao, :numero_autorizacao, :responsavel_autorizacao)
    end
end
