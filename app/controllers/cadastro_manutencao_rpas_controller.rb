class CadastroManutencaoRpasController < ApplicationController
  before_action :set_cadastro_manutencao_rpa, only: [:show, :edit, :update, :destroy]

  # GET /cadastro_manutencao_rpas
  # GET /cadastro_manutencao_rpas.json
  def index
    @cadastro_manutencao_rpas = CadastroManutencaoRpa.all
  end

  # GET /cadastro_manutencao_rpas/1
  # GET /cadastro_manutencao_rpas/1.json
  def show
  end

  # GET /cadastro_manutencao_rpas/new
  def new
    @cadastro_rpas = CadastroRpa.all
    @cadastro_manutencao_rpa = CadastroManutencaoRpa.new
  end

  # GET /cadastro_manutencao_rpas/1/edit
  def edit
    @cadastro_rpas = CadastroRpa.all
  end

  # POST /cadastro_manutencao_rpas
  # POST /cadastro_manutencao_rpas.json
  def create
    @cadastro_manutencao_rpa = CadastroManutencaoRpa.new(cadastro_manutencao_rpa_params)

    respond_to do |format|
      if @cadastro_manutencao_rpa.save
        format.html { redirect_to @cadastro_manutencao_rpa, notice: 'Cadastro manutencao rpa was successfully created.' }
        format.json { render :show, status: :created, location: @cadastro_manutencao_rpa }
      else
        format.html { render :new }
        format.json { render json: @cadastro_manutencao_rpa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cadastro_manutencao_rpas/1
  # PATCH/PUT /cadastro_manutencao_rpas/1.json
  def update
    respond_to do |format|
      if @cadastro_manutencao_rpa.update(cadastro_manutencao_rpa_params)
        format.html { redirect_to @cadastro_manutencao_rpa, notice: 'Cadastro manutencao rpa was successfully updated.' }
        format.json { render :show, status: :ok, location: @cadastro_manutencao_rpa }
      else
        format.html { render :edit }
        format.json { render json: @cadastro_manutencao_rpa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cadastro_manutencao_rpas/1
  # DELETE /cadastro_manutencao_rpas/1.json
  def destroy
    @cadastro_manutencao_rpa.destroy
    respond_to do |format|
      format.html { redirect_to cadastro_manutencao_rpas_url, notice: 'Cadastro manutencao rpa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_manutencao_rpa
      @cadastro_manutencao_rpa = CadastroManutencaoRpa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cadastro_manutencao_rpa_params
      params.require(:cadastro_manutencao_rpa).permit(:nome_guarnicao, :email, :endereco, :telefone, :modelo_rpa, :manutencao_radio, :manutencao_bussola, :manutencao_acelerometro, :manutencao_giroscopio, :manutencao_firmware, :manutencao_helice, :manutencao_prot_helice, :manutencao_carcaca, :manutencao_controle_remoto, :manutencao_telemetria, :manutencao_gps, :manutencao_gimbal, :tipo_controladora_rpa, :outro_problema, :id_usuario, :id_gup)
    end
end
