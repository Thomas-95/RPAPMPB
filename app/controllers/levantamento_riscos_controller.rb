class LevantamentoRiscosController < ApplicationController
  before_action :set_levantamento_risco, only: [:show, :edit, :update, :destroy]

  # GET /levantamento_riscos
  # GET /levantamento_riscos.json
  def index
    @levantamento_riscos = LevantamentoRisco.all
  end

  # GET /levantamento_riscos/1
  # GET /levantamento_riscos/1.json
  def show
  end

  # GET /levantamento_riscos/new
  def new
    @events = Event.all
    @levantamento_risco = LevantamentoRisco.new
  end

  # GET /levantamento_riscos/1/edit
  def edit
    @events = Event.all
  end

  # POST /levantamento_riscos
  # POST /levantamento_riscos.json
  def create
    @levantamento_risco = LevantamentoRisco.new(levantamento_risco_params)

    respond_to do |format|
      if @levantamento_risco.save
        format.html { redirect_to @levantamento_risco, notice: 'Levantamento risco was successfully created.' }
        format.json { render :show, status: :created, location: @levantamento_risco }
      else
        format.html { render :new }
        format.json { render json: @levantamento_risco.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /levantamento_riscos/1
  # PATCH/PUT /levantamento_riscos/1.json
  def update
    respond_to do |format|
      if @levantamento_risco.update(levantamento_risco_params)
        format.html { redirect_to @levantamento_risco, notice: 'Levantamento risco was successfully updated.' }
        format.json { render :show, status: :ok, location: @levantamento_risco }
      else
        format.html { render :edit }
        format.json { render json: @levantamento_risco.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /levantamento_riscos/1
  # DELETE /levantamento_riscos/1.json
  def destroy
    @levantamento_risco.destroy
    respond_to do |format|
      format.html { redirect_to levantamento_riscos_url, notice: 'Levantamento risco was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_levantamento_risco
      @levantamento_risco = LevantamentoRisco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def levantamento_risco_params
      params.require(:levantamento_risco).permit(:condicoes_climaticas, :quantidade_pessoas, :perimetro_voo)
    end
end
