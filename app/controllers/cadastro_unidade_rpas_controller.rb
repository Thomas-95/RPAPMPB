class CadastroUnidadeRpasController < ApplicationController
  before_action :set_cadastro_unidade_rpa, only: [:show, :edit, :update, :destroy]

  # GET /cadastro_unidade_rpas
  # GET /cadastro_unidade_rpas.json
  def index
    @cadastro_unidade_rpas = CadastroUnidadeRpa.all
  end

  # GET /cadastro_unidade_rpas/1
  # GET /cadastro_unidade_rpas/1.json
  def show
  end

  # GET /cadastro_unidade_rpas/new
  def new
    @cadastro_rpas = CadastroRpa.all
    @cadastro_unidade_rpa = CadastroUnidadeRpa.new
  end

  # GET /cadastro_unidade_rpas/1/edit
  def edit
    @cadastro_rpas = CadastroRpa.all
  end

  # POST /cadastro_unidade_rpas
  # POST /cadastro_unidade_rpas.json
  def create
    @cadastro_unidade_rpa = CadastroUnidadeRpa.new(cadastro_unidade_rpa_params)

    respond_to do |format|
      if @cadastro_unidade_rpa.save
        format.html { redirect_to @cadastro_unidade_rpa, notice: 'Cadastro unidade rpa was successfully created.' }
        format.json { render :show, status: :created, location: @cadastro_unidade_rpa }
      else
        format.html { render :new }
        format.json { render json: @cadastro_unidade_rpa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cadastro_unidade_rpas/1
  # PATCH/PUT /cadastro_unidade_rpas/1.json
  def update
    respond_to do |format|
      if @cadastro_unidade_rpa.update(cadastro_unidade_rpa_params)
        format.html { redirect_to @cadastro_unidade_rpa, notice: 'Cadastro unidade rpa was successfully updated.' }
        format.json { render :show, status: :ok, location: @cadastro_unidade_rpa }
      else
        format.html { render :edit }
        format.json { render json: @cadastro_unidade_rpa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cadastro_unidade_rpas/1
  # DELETE /cadastro_unidade_rpas/1.json
  def destroy
    @cadastro_unidade_rpa.destroy
    respond_to do |format|
      format.html { redirect_to cadastro_unidade_rpas_url, notice: 'Cadastro unidade rpa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_unidade_rpa
      @cadastro_unidade_rpa = CadastroUnidadeRpa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cadastro_unidade_rpa_params
      params.require(:cadastro_unidade_rpa).permit(:identificador_rpa, :modelo_rpa, :comando_policiamento_area, :regiao_policia_militar, :pelotao_policia_militar, :destac_policial_ostensivo, :destac_policial_militar, :posto_policiamento_comum, :base_policiamento_comum, :grupamento_especial, :guarnicoes, :companhia_indepen_policia, :id_cm)
    end
end
