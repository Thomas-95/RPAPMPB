json.extract! levantamento_risco, :id, :condicoes_climaticas, :quantidade_pessoas, :perimetro_voo, :created_at, :updated_at
json.url levantamento_risco_url(levantamento_risco, format: :json)