json.extract! autorizacao_voo, :id, :tipo_autorizacao, :numero_autorizacao, :responsavel_autorizacao, :created_at, :updated_at
json.url autorizacao_voo_url(autorizacao_voo, format: :json)