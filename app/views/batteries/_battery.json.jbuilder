json.extract! battery, :id, :serial_sigla, :serial_numero, :compativel_aeronave, :modelo, :material, :carga, :potencia, :tensao, :created_at, :updated_at
json.url battery_url(battery, format: :json)