json.extract! acessorio, :id, :nome, :cor, :descricao, :created_at, :updated_at
json.url acessorio_url(acessorio, format: :json)