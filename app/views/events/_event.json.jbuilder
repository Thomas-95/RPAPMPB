json.extract! event, :id, :nome_evento, :coordenada_geo, :cidade, :endereco, :data, :duracao, :created_at, :updated_at
json.url event_url(event, format: :json)