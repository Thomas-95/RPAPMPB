class Event < ApplicationRecord

  validates :nome_evento,
            :coordenada_geo,
            :cidade,
            :endereco,
            :duracao,
            presence: true

end
