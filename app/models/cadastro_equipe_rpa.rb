class CadastroEquipeRpa < ApplicationRecord
  
  validates :nome,
            :tipo_licenca_certificado,
            :numero_licenca_certificado,
            :anexar_copia,
            :experiencia_pilito_observador,
            presence: true
end
