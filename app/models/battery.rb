class Battery < ApplicationRecord

  validates :serial_sigla,
            :modelo,
            :material,
            :carga,
            :tensao,
            presence: true


end
