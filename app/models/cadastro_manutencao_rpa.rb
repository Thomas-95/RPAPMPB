class CadastroManutencaoRpa < ApplicationRecord

  validates :nome_guarnicao,
            :manutencao_radio,
            :manutencao_bussola,
            :manutencao_acelerometro,
            :manutencao_giroscopio,
            :manutencao_firmware,
            :manutencao_helice,
            :manutencao_prot_helice,
            :manutencao_carcaca,
            :manutencao_controle_remoto,
            :manutencao_telemetria,
            :manutencao_gps,
            :manutencao_gimbal,
            :tipo_controladora_rpa,
            :outro_problema,
            presence: true
end
