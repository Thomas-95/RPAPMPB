class LevantamentoRisco < ApplicationRecord

  validates :condicoes_climaticas,
            :quantidade_pessoas,
            :perimetro_voo,
            presence: true

end
