class PlanoDeVoo < ApplicationRecord

  validates :objetivo_operacao,
            :data_voo,
            :duracao_frequencia_voo,
            :regra_voo,
            :tipo_operacao,
            :numero_estacao,
            :localizao_estacao,
            :procedimento_transferencia,
            :local_decolagem,
            :local_destino,
            :requisitos_decolagem,
            :requisitos_pouso,
            :rota,
            :altura_voo,
            :comunicacao_ats,
            :enlace_pilotagem,
            :comunicacao_piloto_observador,
            :seguranca,
            :auxiliar,
            :observador,
            presence: true
end
