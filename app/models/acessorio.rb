class Acessorio < ApplicationRecord

    validates :nome,
              :cor,
              :descricao,
              presence: true

end
