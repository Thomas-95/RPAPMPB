class AutorizacaoVoo < ApplicationRecord

  validates :tipo_autorizacao,
            :numero_autorizacao,
            :responsavel_autorizacao,
            presence: true


end
