class RelatorioPlanoDeVoo < ApplicationRecord

  validates :condições_relevantes,
            :discrep_obser_missao,
            :discrep_anteriores_missao,
            :conclusao,
            presence: true
end
