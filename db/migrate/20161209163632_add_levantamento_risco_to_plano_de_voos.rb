class AddLevantamentoRiscoToPlanoDeVoos < ActiveRecord::Migration[5.0]
  def change
    add_reference :plano_de_voos, :levantamento_risco, foreign_key: true
  end
end
