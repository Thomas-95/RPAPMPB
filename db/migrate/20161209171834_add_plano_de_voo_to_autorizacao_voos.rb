class AddPlanoDeVooToAutorizacaoVoos < ActiveRecord::Migration[5.0]
  def change
    add_reference :autorizacao_voos, :plano_de_voo, foreign_key: true
  end
end
