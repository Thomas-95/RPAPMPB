class CreateAcessorios < ActiveRecord::Migration[5.0]
  def change
    create_table :acessorios do |t|
      t.string :nome
      t.string :cor
      t.text :descricao

      t.timestamps
    end
  end
end
