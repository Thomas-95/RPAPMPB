class AddFieldsToCadastroRpa < ActiveRecord::Migration[5.0]
  def change
    add_column :cadastro_rpas, :classe, :string
    add_column :cadastro_rpas, :anexar_ruido, :string
    add_column :cadastro_rpas, :tipo_aeronave, :string
    add_column :cadastro_rpas, :peso_maximo_decolagem, :string
    add_column :cadastro_rpas, :esteria_turbulencia, :string
    add_column :cadastro_rpas, :numero_motor, :integer
    add_column :cadastro_rpas, :tipos_motor, :string
    add_column :cadastro_rpas, :dimensoes_rpa, :string
    add_column :cadastro_rpas, :velocidade_maxima, :string
    add_column :cadastro_rpas, :velocidade_minima, :string
    add_column :cadastro_rpas, :velocidade_cruzeiro, :string
    add_column :cadastro_rpas, :razoes_subida_maxima, :text
    add_column :cadastro_rpas, :razoes_descida_maxima, :text
    add_column :cadastro_rpas, :autonomia_maxima, :string
    add_column :cadastro_rpas, :capacidade_detectar_evitar, :text
    add_column :cadastro_rpas, :carga_util, :text
    add_column :cadastro_rpas, :enlace_dados_carga, :text
    add_column :cadastro_rpas, :numero_seguro, :text
    add_column :cadastro_rpas, :outros_dados, :text
    add_column :cadastro_rpas, :tipo_cns, :text
    add_column :cadastro_rpas, :telefonia_cns, :text
    add_column :cadastro_rpas, :navegacao_cns, :text
    add_column :cadastro_rpas, :vigilancia_cns, :text
    add_column :cadastro_rpas, :modo_cns, :text
    add_column :cadastro_rpas, :outros_cns, :text
  end
end
