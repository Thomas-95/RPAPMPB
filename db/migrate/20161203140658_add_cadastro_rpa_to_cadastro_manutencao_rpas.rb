class AddCadastroRpaToCadastroManutencaoRpas < ActiveRecord::Migration[5.0]
  def change
    add_reference :cadastro_manutencao_rpas, :cadastro_rpa, foreign_key: true
  end
end
