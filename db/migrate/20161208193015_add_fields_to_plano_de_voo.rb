class AddFieldsToPlanoDeVoo < ActiveRecord::Migration[5.0]
  def change
    add_column :plano_de_voos, :seguranca, :string
    add_column :plano_de_voos, :auxiliar, :string
    add_column :plano_de_voos, :observador, :string
  end
end
