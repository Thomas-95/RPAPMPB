class CreateAutorizacaoVoos < ActiveRecord::Migration[5.0]
  def change
    create_table :autorizacao_voos do |t|
      t.string :tipo_autorizacao
      t.integer :numero_autorizacao
      t.string :responsavel_autorizacao

      t.timestamps
    end
  end
end
