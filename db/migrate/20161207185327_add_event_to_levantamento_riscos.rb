class AddEventToLevantamentoRiscos < ActiveRecord::Migration[5.0]
  def change
    add_reference :levantamento_riscos, :event, foreign_key: true
  end
end
