class CreateBatteries < ActiveRecord::Migration[5.0]
  def change
    create_table :batteries do |t|
      t.string :serial_sigla
      t.string :serial_numero
      t.string :compativel_aeronave
      t.string :modelo
      t.string :material
      t.string :carga
      t.string :potencia
      t.string :tensao

      t.timestamps
    end
  end
end
