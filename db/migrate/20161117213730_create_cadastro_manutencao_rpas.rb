class CreateCadastroManutencaoRpas < ActiveRecord::Migration[5.0]
  def change
    create_table :cadastro_manutencao_rpas do |t|
      t.string :nome_guarnicao
      t.string :email
      t.string :endereco
      t.string :telefone
      t.string :modelo_rpa
      t.string :manutencao_radio
      t.string :manutencao_bussola
      t.string :manutencao_acelerometro
      t.string :manutencao_giroscopio
      t.string :manutencao_firmware
      t.string :manutencao_helice
      t.string :manutencao_prot_helice
      t.string :manutencao_carcaca
      t.string :manutencao_controle_remoto
      t.string :manutencao_telemetria
      t.string :manutencao_gps
      t.string :manutencao_gimbal
      t.string :tipo_controladora_rpa
      t.text :outro_problema
      t.integer :id_usuario
      t.integer :id_gup

      t.timestamps
    end
  end
end
