class AddCadastroRpaToAcessorios < ActiveRecord::Migration[5.0]
  def change
    add_reference :acessorios, :cadastro_rpa, foreign_key: true
  end
end
