class AddPlanoDeVooToCadastroEquipeRpas < ActiveRecord::Migration[5.0]
  def change
    add_reference :cadastro_equipe_rpas, :plano_de_voo, foreign_key: true
  end
end
