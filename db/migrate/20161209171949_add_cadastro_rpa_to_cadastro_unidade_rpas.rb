class AddCadastroRpaToCadastroUnidadeRpas < ActiveRecord::Migration[5.0]
  def change
    add_reference :cadastro_unidade_rpas, :cadastro_rpa, foreign_key: true
  end
end
