class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :nome_evento
      t.string :coordenada_geo
      t.string :cidade
      t.string :endereco
      t.datetime :data
      t.string :duracao

      t.timestamps
    end
  end
end
