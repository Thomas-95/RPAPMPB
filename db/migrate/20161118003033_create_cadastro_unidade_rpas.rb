class CreateCadastroUnidadeRpas < ActiveRecord::Migration[5.0]
  def change
    create_table :cadastro_unidade_rpas do |t|
      t.string :identificador_rpa
      t.string :modelo_rpa
      t.string :comando_policiamento_area
      t.string :regiao_policia_militar
      t.string :pelotao_policia_militar
      t.string :destac_policial_ostensivo
      t.string :destac_policial_militar
      t.string :posto_policiamento_comum
      t.string :base_policiamento_comum
      t.string :grupamento_especial
      t.string :guarnicoes
      t.string :companhia_indepen_policia
      t.integer :id_cm

      t.timestamps
    end
  end
end
