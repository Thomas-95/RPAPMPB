class CreateLevantamentoRiscos < ActiveRecord::Migration[5.0]
  def change
    create_table :levantamento_riscos do |t|
      t.string :condicoes_climaticas
      t.integer :quantidade_pessoas
      t.string :perimetro_voo

      t.timestamps
    end
  end
end
