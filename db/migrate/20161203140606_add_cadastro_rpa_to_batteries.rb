class AddCadastroRpaToBatteries < ActiveRecord::Migration[5.0]
  def change
    add_reference :batteries, :cadastro_rpa, foreign_key: true
  end
end
