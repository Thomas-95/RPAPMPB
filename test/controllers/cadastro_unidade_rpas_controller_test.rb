require 'test_helper'

class CadastroUnidadeRpasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cadastro_unidade_rpa = cadastro_unidade_rpas(:one)
  end

  test "should get index" do
    get cadastro_unidade_rpas_url
    assert_response :success
  end

  test "should get new" do
    get new_cadastro_unidade_rpa_url
    assert_response :success
  end

  test "should create cadastro_unidade_rpa" do
    assert_difference('CadastroUnidadeRpa.count') do
      post cadastro_unidade_rpas_url, params: { cadastro_unidade_rpa: { base_policiamento_comum: @cadastro_unidade_rpa.base_policiamento_comum, comando_policiamento_area: @cadastro_unidade_rpa.comando_policiamento_area, companhia_indepen_policia: @cadastro_unidade_rpa.companhia_indepen_policia, destac_policial_militar: @cadastro_unidade_rpa.destac_policial_militar, destac_policial_ostensivo: @cadastro_unidade_rpa.destac_policial_ostensivo, grupamento_especial: @cadastro_unidade_rpa.grupamento_especial, guarnicoes: @cadastro_unidade_rpa.guarnicoes, id_cm: @cadastro_unidade_rpa.id_cm, identificador_rpa: @cadastro_unidade_rpa.identificador_rpa, modelo_rpa: @cadastro_unidade_rpa.modelo_rpa, pelotao_policia_militar: @cadastro_unidade_rpa.pelotao_policia_militar, posto_policiamento_comum: @cadastro_unidade_rpa.posto_policiamento_comum, regiao_policia_militar: @cadastro_unidade_rpa.regiao_policia_militar } }
    end

    assert_redirected_to cadastro_unidade_rpa_url(CadastroUnidadeRpa.last)
  end

  test "should show cadastro_unidade_rpa" do
    get cadastro_unidade_rpa_url(@cadastro_unidade_rpa)
    assert_response :success
  end

  test "should get edit" do
    get edit_cadastro_unidade_rpa_url(@cadastro_unidade_rpa)
    assert_response :success
  end

  test "should update cadastro_unidade_rpa" do
    patch cadastro_unidade_rpa_url(@cadastro_unidade_rpa), params: { cadastro_unidade_rpa: { base_policiamento_comum: @cadastro_unidade_rpa.base_policiamento_comum, comando_policiamento_area: @cadastro_unidade_rpa.comando_policiamento_area, companhia_indepen_policia: @cadastro_unidade_rpa.companhia_indepen_policia, destac_policial_militar: @cadastro_unidade_rpa.destac_policial_militar, destac_policial_ostensivo: @cadastro_unidade_rpa.destac_policial_ostensivo, grupamento_especial: @cadastro_unidade_rpa.grupamento_especial, guarnicoes: @cadastro_unidade_rpa.guarnicoes, id_cm: @cadastro_unidade_rpa.id_cm, identificador_rpa: @cadastro_unidade_rpa.identificador_rpa, modelo_rpa: @cadastro_unidade_rpa.modelo_rpa, pelotao_policia_militar: @cadastro_unidade_rpa.pelotao_policia_militar, posto_policiamento_comum: @cadastro_unidade_rpa.posto_policiamento_comum, regiao_policia_militar: @cadastro_unidade_rpa.regiao_policia_militar } }
    assert_redirected_to cadastro_unidade_rpa_url(@cadastro_unidade_rpa)
  end

  test "should destroy cadastro_unidade_rpa" do
    assert_difference('CadastroUnidadeRpa.count', -1) do
      delete cadastro_unidade_rpa_url(@cadastro_unidade_rpa)
    end

    assert_redirected_to cadastro_unidade_rpas_url
  end
end
