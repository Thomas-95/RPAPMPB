require 'test_helper'

class AutorizacaoVoosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @autorizacao_voo = autorizacao_voos(:one)
  end

  test "should get index" do
    get autorizacao_voos_url
    assert_response :success
  end

  test "should get new" do
    get new_autorizacao_voo_url
    assert_response :success
  end

  test "should create autorizacao_voo" do
    assert_difference('AutorizacaoVoo.count') do
      post autorizacao_voos_url, params: { autorizacao_voo: { numero_autorizacao: @autorizacao_voo.numero_autorizacao, responsavel_autorizacao: @autorizacao_voo.responsavel_autorizacao, tipo_autorizacao: @autorizacao_voo.tipo_autorizacao } }
    end

    assert_redirected_to autorizacao_voo_url(AutorizacaoVoo.last)
  end

  test "should show autorizacao_voo" do
    get autorizacao_voo_url(@autorizacao_voo)
    assert_response :success
  end

  test "should get edit" do
    get edit_autorizacao_voo_url(@autorizacao_voo)
    assert_response :success
  end

  test "should update autorizacao_voo" do
    patch autorizacao_voo_url(@autorizacao_voo), params: { autorizacao_voo: { numero_autorizacao: @autorizacao_voo.numero_autorizacao, responsavel_autorizacao: @autorizacao_voo.responsavel_autorizacao, tipo_autorizacao: @autorizacao_voo.tipo_autorizacao } }
    assert_redirected_to autorizacao_voo_url(@autorizacao_voo)
  end

  test "should destroy autorizacao_voo" do
    assert_difference('AutorizacaoVoo.count', -1) do
      delete autorizacao_voo_url(@autorizacao_voo)
    end

    assert_redirected_to autorizacao_voos_url
  end
end
