require 'test_helper'

class LevantamentoRiscosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @levantamento_risco = levantamento_riscos(:one)
  end

  test "should get index" do
    get levantamento_riscos_url
    assert_response :success
  end

  test "should get new" do
    get new_levantamento_risco_url
    assert_response :success
  end

  test "should create levantamento_risco" do
    assert_difference('LevantamentoRisco.count') do
      post levantamento_riscos_url, params: { levantamento_risco: { condicoes_climaticas: @levantamento_risco.condicoes_climaticas, perimetro_voo: @levantamento_risco.perimetro_voo, quantidade_pessoas: @levantamento_risco.quantidade_pessoas } }
    end

    assert_redirected_to levantamento_risco_url(LevantamentoRisco.last)
  end

  test "should show levantamento_risco" do
    get levantamento_risco_url(@levantamento_risco)
    assert_response :success
  end

  test "should get edit" do
    get edit_levantamento_risco_url(@levantamento_risco)
    assert_response :success
  end

  test "should update levantamento_risco" do
    patch levantamento_risco_url(@levantamento_risco), params: { levantamento_risco: { condicoes_climaticas: @levantamento_risco.condicoes_climaticas, perimetro_voo: @levantamento_risco.perimetro_voo, quantidade_pessoas: @levantamento_risco.quantidade_pessoas } }
    assert_redirected_to levantamento_risco_url(@levantamento_risco)
  end

  test "should destroy levantamento_risco" do
    assert_difference('LevantamentoRisco.count', -1) do
      delete levantamento_risco_url(@levantamento_risco)
    end

    assert_redirected_to levantamento_riscos_url
  end
end
