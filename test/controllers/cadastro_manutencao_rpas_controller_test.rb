require 'test_helper'

class CadastroManutencaoRpasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cadastro_manutencao_rpa = cadastro_manutencao_rpas(:one)
  end

  test "should get index" do
    get cadastro_manutencao_rpas_url
    assert_response :success
  end

  test "should get new" do
    get new_cadastro_manutencao_rpa_url
    assert_response :success
  end

  test "should create cadastro_manutencao_rpa" do
    assert_difference('CadastroManutencaoRpa.count') do
      post cadastro_manutencao_rpas_url, params: { cadastro_manutencao_rpa: { email: @cadastro_manutencao_rpa.email, endereco: @cadastro_manutencao_rpa.endereco, id_gup: @cadastro_manutencao_rpa.id_gup, id_usuario: @cadastro_manutencao_rpa.id_usuario, manutencao_acelerometro: @cadastro_manutencao_rpa.manutencao_acelerometro, manutencao_bussola: @cadastro_manutencao_rpa.manutencao_bussola, manutencao_carcaca: @cadastro_manutencao_rpa.manutencao_carcaca, manutencao_controle_remoto: @cadastro_manutencao_rpa.manutencao_controle_remoto, manutencao_firmware: @cadastro_manutencao_rpa.manutencao_firmware, manutencao_gimbal: @cadastro_manutencao_rpa.manutencao_gimbal, manutencao_giroscopio: @cadastro_manutencao_rpa.manutencao_giroscopio, manutencao_gps: @cadastro_manutencao_rpa.manutencao_gps, manutencao_helice: @cadastro_manutencao_rpa.manutencao_helice, manutencao_prot_helice: @cadastro_manutencao_rpa.manutencao_prot_helice, manutencao_radio: @cadastro_manutencao_rpa.manutencao_radio, manutencao_telemetria: @cadastro_manutencao_rpa.manutencao_telemetria, modelo_rpa: @cadastro_manutencao_rpa.modelo_rpa, nome_guarnicao: @cadastro_manutencao_rpa.nome_guarnicao, outro_problema: @cadastro_manutencao_rpa.outro_problema, telefone: @cadastro_manutencao_rpa.telefone, tipo_controladora_rpa: @cadastro_manutencao_rpa.tipo_controladora_rpa } }
    end

    assert_redirected_to cadastro_manutencao_rpa_url(CadastroManutencaoRpa.last)
  end

  test "should show cadastro_manutencao_rpa" do
    get cadastro_manutencao_rpa_url(@cadastro_manutencao_rpa)
    assert_response :success
  end

  test "should get edit" do
    get edit_cadastro_manutencao_rpa_url(@cadastro_manutencao_rpa)
    assert_response :success
  end

  test "should update cadastro_manutencao_rpa" do
    patch cadastro_manutencao_rpa_url(@cadastro_manutencao_rpa), params: { cadastro_manutencao_rpa: { email: @cadastro_manutencao_rpa.email, endereco: @cadastro_manutencao_rpa.endereco, id_gup: @cadastro_manutencao_rpa.id_gup, id_usuario: @cadastro_manutencao_rpa.id_usuario, manutencao_acelerometro: @cadastro_manutencao_rpa.manutencao_acelerometro, manutencao_bussola: @cadastro_manutencao_rpa.manutencao_bussola, manutencao_carcaca: @cadastro_manutencao_rpa.manutencao_carcaca, manutencao_controle_remoto: @cadastro_manutencao_rpa.manutencao_controle_remoto, manutencao_firmware: @cadastro_manutencao_rpa.manutencao_firmware, manutencao_gimbal: @cadastro_manutencao_rpa.manutencao_gimbal, manutencao_giroscopio: @cadastro_manutencao_rpa.manutencao_giroscopio, manutencao_gps: @cadastro_manutencao_rpa.manutencao_gps, manutencao_helice: @cadastro_manutencao_rpa.manutencao_helice, manutencao_prot_helice: @cadastro_manutencao_rpa.manutencao_prot_helice, manutencao_radio: @cadastro_manutencao_rpa.manutencao_radio, manutencao_telemetria: @cadastro_manutencao_rpa.manutencao_telemetria, modelo_rpa: @cadastro_manutencao_rpa.modelo_rpa, nome_guarnicao: @cadastro_manutencao_rpa.nome_guarnicao, outro_problema: @cadastro_manutencao_rpa.outro_problema, telefone: @cadastro_manutencao_rpa.telefone, tipo_controladora_rpa: @cadastro_manutencao_rpa.tipo_controladora_rpa } }
    assert_redirected_to cadastro_manutencao_rpa_url(@cadastro_manutencao_rpa)
  end

  test "should destroy cadastro_manutencao_rpa" do
    assert_difference('CadastroManutencaoRpa.count', -1) do
      delete cadastro_manutencao_rpa_url(@cadastro_manutencao_rpa)
    end

    assert_redirected_to cadastro_manutencao_rpas_url
  end
end
